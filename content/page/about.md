---
title: A little bit more about me
comments: false
---

My name is Xun Zhu. I'm a bioinformatician currently working in Hawaii. If you
care about my research, here is [my Google Scholar
profile](https://scholar.google.com/citations?user=iMHMSRsAAAAJ&hl=en).
