Hey there! You have landed on Xun's little place to put some of his random stuff.

I like [coding](https://gitlab.com/xz),
[drawing](https://www.deviantart.com/zhuxun2), and [transcribe music by
ear](https://musescore.com/user/118644).

Here are things I built that might be useful to you.

  * [vll](https://github.com/w9/vll-haskell): an ultra fast commandline csv/tsv
    viewer specially designed for huge files
  * [zp](http://60g.org/zp): a WebGL-powered 3d scatter-plot engine with elegant
    visual design
  * [dati](http://60g.org/dati): an a toy dataset painter

I sometimes write [random stuff]({{< ref "post" >}}), so take a look if you feel like.
